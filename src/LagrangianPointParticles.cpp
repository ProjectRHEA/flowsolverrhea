#include "LagrangianPointParticles.hpp"

using namespace std;

////////// FIXED PARAMETERS //////////
const double pi = 2.0*asin( 1.0 );				/// pi number (fixed)


////////// BaseLagrangianPointParticle CLASS //////////

BaseLagrangianPointParticle::BaseLagrangianPointParticle() {};
        
BaseLagrangianPointParticle::BaseLagrangianPointParticle(const string configuration_file) {

    /// Read configuration (input) file
    this->readConfigurationFile( configuration_file );

    /// Default initialization of identifier and position ... they will be overwritten later
    identifier_prt = -1;
    position_prt[0] = 0.0; position_prt[1] = 0.0; position_prt[2] = 0.0;
    position_0_prt[0] = 0.0; position_0_prt[1] = 0.0; position_0_prt[2] = 0.0;

    /// Default initialization of velocity to zero ... it can be modified in the future 
    velocity_prt[0] = 0.0; velocity_prt[1] = 0.0; velocity_prt[2] = 0.0;
    velocity_0_prt[0] = 0.0; velocity_0_prt[1] = 0.0; velocity_0_prt[2] = 0.0;

};

BaseLagrangianPointParticle::~BaseLagrangianPointParticle() {};

void BaseLagrangianPointParticle::readConfigurationFile( const string configuration_file ) {

    /// Create YAML object
    YAML::Node configuration = YAML::LoadFile( configuration_file );

    /// Lagrangian point particles
    const YAML::Node & lagrangian_point_particles = configuration["lagrangian_point_particles"];
    diameter_prt = lagrangian_point_particles["diameter_particles"].as<double>();
    density_prt  = lagrangian_point_particles["density_particles"].as<double>();

};

double BaseLagrangianPointParticle::calculateVolumePrt() {

    /// Assuming spherical particle
    double volume_prt = ( pi/6.0 )*pow( diameter_prt, 3.0 );

    return volume_prt;

};

double BaseLagrangianPointParticle::calculateMassPrt() {

    /// Assuming spherical particle
    double mass_prt = ( pi/6.0 )*density_prt*pow( diameter_prt, 3.0 );

    return mass_prt;

};

double BaseLagrangianPointParticle::calculateRelaxationTimePrt(const double &mu_fluid) {

    /// Assuming spherical particle
    double relaxation_time_prt = ( density_prt*pow( diameter_prt, 2.0 ) )/( 18.0*mu_fluid );

    return relaxation_time_prt;

};

double BaseLagrangianPointParticle::calculateDensityRatioBetaPrt(const double &rho_fluid) {

    /// Assuming point particle
    double density_ratio_prt = ( 3.0*rho_fluid )/( rho_fluid + 2.0*density_prt );

    return density_ratio_prt;

};

////////// DistributedPointParticles CLASS //////////
