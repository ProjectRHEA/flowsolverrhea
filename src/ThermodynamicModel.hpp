#ifndef _THERMODYNAMIC_MODEL_
#define _THERMODYNAMIC_MODEL_

////////// INCLUDES //////////
#include <stdio.h>
#include <cmath>
#include <iostream>
#include <complex>
#include <limits>
#include <mpi.h>
#include "yaml-cpp/yaml.h"
#include "RootFindingMinimization.hpp"

////////// CLASS DECLARATION //////////
class BaseThermodynamicModel;				/// Base thermodynamic model
class IdealGasModel;					/// Ideal-gas thermodynamic model
class StiffenedGasModel;				/// Stiffened-gas thermodynamic model
class PengRobinsonModel;				/// Peng-Robinson (real-gas) thermodynamic model

////////// FUNCTION DECLARATION //////////


////////// BaseThermodynamicModel CLASS //////////
class BaseThermodynamicModel {
   
    public:

        ////////// CONSTRUCTORS & DESTRUCTOR //////////
        BaseThermodynamicModel();					/// Default constructor
        BaseThermodynamicModel(const std::string configuration_file);	/// Parametrized constructor
        virtual ~BaseThermodynamicModel();				/// Destructor

	////////// GET FUNCTIONS //////////
	inline double getSpecificGasConstant() { return( R_specific ); };
	inline double getMolecularWeight() { return( molecular_weight ); };

	////////// SET FUNCTIONS //////////
        inline void setSpecificGasConstant(double R_specific_) { R_specific = R_specific_; };
        inline void setMolecularWeight(double molecular_weight_) { molecular_weight = molecular_weight_; };

	////////// METHODS //////////
       
        /// Read configuration (input) file written in YAML language
        //virtual void readConfigurationFile() {};
        void readConfigurationFile() {};

        /// Calculate temperature from pressure and density
        //virtual double calculateTemperatureFromPressureDensity(const double &P, const double &rho) = 0;
	#pragma acc routine
        double calculateTemperatureFromPressureDensity(const double &P, const double &rho) { return 0.0; };	// ... modified for OpenACC

        /// Calculate temperature from pressure and density with initial guess
        //virtual void calculateTemperatureFromPressureDensityWithInitialGuess(double &T, const double &P, const double &rho) {};
	#pragma acc routine
        void calculateTemperatureFromPressureDensityWithInitialGuess(double &T, const double &P, const double &rho) {};	// ... modified for OpenACC

        /// Calculate pressure from temperature and density
        //virtual double calculatePressureFromTemperatureDensity(const double &T, const double &rho) = 0;
	#pragma acc routine
        double calculatePressureFromTemperatureDensity(const double &T, const double &rho) { return 0.0; };	// ... modified for OpenACC

        /// Calculate internal energy from pressure, temperature and density
        //virtual double calculateInternalEnergyFromPressureTemperatureDensity(const double &P, const double &T, const double &rho) = 0;
	#pragma acc routine
        double calculateInternalEnergyFromPressureTemperatureDensity(const double &P, const double &T, const double &rho) { return 0.0; };	// ... modified for OpenACC

        /// Calculate pressure and temperature from density and internal energy
        //virtual void calculatePressureTemperatureFromDensityInternalEnergy(double &P, double &T, const double &rho, const double &e) {};
	#pragma acc routine
        void calculatePressureTemperatureFromDensityInternalEnergy(double &P, double &T, const double &rho, const double &e) {};	// ... modified for OpenACC
        
        /// Calculate density from pressure and temperature
        //virtual double calculateDensityFromPressureTemperature(const double &P, const double &T) = 0;
	#pragma acc routine
        double calculateDensityFromPressureTemperature(const double &P, const double &T) { return 0.0; };	// ... modified for OpenACC

        /// Calculate density and internal energy from pressure and temperature
        //virtual void calculateDensityInternalEnergyFromPressureTemperature(double &rho, double &e, const double &P, const double &T) {};
	#pragma acc routine
        void calculateDensityInternalEnergyFromPressureTemperature(double &rho, double &e, const double &P, const double &T) {};	// ... modified for OpenACC

        /// Calculate specific heat capacities
        //virtual void calculateSpecificHeatCapacities(double &c_v, double &c_p, const double &P, const double &T, const double &rho) {};
	#pragma acc routine
        void calculateSpecificHeatCapacities(double &c_v, double &c_p, const double &P, const double &T, const double &rho) {};	// ... modified for OpenACC

        /// Calculate heat capacities ratio
        //virtual double calculateHeatCapacitiesRatio(const double &P, const double &rho) = 0;
	#pragma acc routine
        double calculateHeatCapacitiesRatio(const double &P, const double &rho) { return 0.0; };	// ... modified for OpenACC

        /// Calculate speed of sound
        //virtual double calculateSoundSpeed(const double &P, const double &T, const double &rho) = 0;
	#pragma acc routine
        double calculateSoundSpeed(const double &P, const double &T, const double &rho) { return 0.0; };	// ... modified for OpenACC

        /// Calculate expansivity & compressibility
        //virtual double calculateVolumeExpansivity(const double &T, const double &bar_v) = 0;
	#pragma acc routine
        double calculateVolumeExpansivity(const double &T, const double &bar_v) { return 0.0; };	// ... modified for OpenACC
        //virtual double calculateIsothermalCompressibility(const double &T, const double &bar_v) = 0;
	#pragma acc routine
        double calculateIsothermalCompressibility(const double &T, const double &bar_v) { return 0.0; };	// ... modified for OpenACC
        //virtual double calculateIsentropicCompressibility(const double &P, const double &T, const double &bar_v) = 0;
	#pragma acc routine
        double calculateIsentropicCompressibility(const double &P, const double &T, const double &bar_v) { return 0.0; };	// ... modified for OpenACC

    protected:

        ////////// PARAMETERS //////////

        /// Thermodynamic properties
        double R_universal = 8.31446261815324;					/// Universal (ideal-gas) gas constant [J/(mol·K)]
        double R_specific;							/// Specific gas constant [J/(kg·K)]
        double molecular_weight;						/// Molecular weight [kg/mol]

        /// Model parameters
	std::string configuration_file;						/// Configuration file name (YAML language)
        
    private:

};


////////// IdealGasModel CLASS //////////
class IdealGasModel : public BaseThermodynamicModel {
   
    public:

        ////////// CONSTRUCTORS & DESTRUCTOR //////////
        IdealGasModel();							/// Default constructor
        IdealGasModel(const std::string configuration_file);			/// Parametrized constructor
        virtual ~IdealGasModel();						/// Destructor

	////////// GET FUNCTIONS //////////

	////////// SET FUNCTIONS //////////

	////////// METHODS //////////
        
        /// Read configuration (input) file written in YAML language
        void readConfigurationFile();

        /// Calculate temperature from pressure and density
	#pragma acc routine
        double calculateTemperatureFromPressureDensity(const double &P, const double &rho);

        /// Calculate temperature from pressure and density with initial guess
	#pragma acc routine
        void calculateTemperatureFromPressureDensityWithInitialGuess(double &T, const double &P, const double &rho);

        /// Calculate pressure from temperature and density
	#pragma acc routine
        double calculatePressureFromTemperatureDensity(const double &T, const double &rho);

        /// Calculate internal energy from pressure, temperature and density
	#pragma acc routine
        double calculateInternalEnergyFromPressureTemperatureDensity(const double &P, const double &T, const double &rho);

        /// Calculate pressure and temperature from density and internal energy
	#pragma acc routine
        void calculatePressureTemperatureFromDensityInternalEnergy(double &P, double &T, const double &rho, const double &e);

        /// Calculate density from pressure and temperature
	#pragma acc routine
        double calculateDensityFromPressureTemperature(const double &P, const double &T);

        /// Calculate density and internal energy from pressure and temperature
	#pragma acc routine
        void calculateDensityInternalEnergyFromPressureTemperature(double &rho, double &e, const double &P, const double &T);

        /// Calculate specific heat capacities
	#pragma acc routine
        void calculateSpecificHeatCapacities(double &c_v, double &c_p, const double &P, const double &T, const double &rho);

        /// Calculate heat capacities ratio
	#pragma acc routine
        double calculateHeatCapacitiesRatio(const double &P, const double &rho);

        /// Calculate speed of sound
	#pragma acc routine
        double calculateSoundSpeed(const double &P, const double &T, const double &rho);

        /// Calculate expansivity & compressibility
	#pragma acc routine
        double calculateVolumeExpansivity(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsothermalCompressibility(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsentropicCompressibility(const double &P, const double &T, const double &bar_v);

    protected:

        ////////// PARAMETERS //////////

        /// Thermodynamic properties
        double gamma;						/// Heat capacities ratio (ideal-gas) [-]

    private:

};


////////// StiffenedGasModel CLASS //////////
class StiffenedGasModel : public BaseThermodynamicModel {
   
    public:

        ////////// CONSTRUCTORS & DESTRUCTOR //////////
        StiffenedGasModel();							/// Default constructor
        StiffenedGasModel(const std::string configuration_file);		/// Parametrized constructor
        virtual ~StiffenedGasModel();						/// Destructor

	////////// GET FUNCTIONS //////////

	////////// SET FUNCTIONS //////////

	////////// METHODS //////////
        
        /// Read configuration (input) file written in YAML language
        void readConfigurationFile();

        /// Calculate temperature from pressure and density
	#pragma acc routine
        double calculateTemperatureFromPressureDensity(const double &P, const double &rho);

        /// Calculate temperature from pressure and density with initial guess
	#pragma acc routine
        void calculateTemperatureFromPressureDensityWithInitialGuess(double &T, const double &P, const double &rho);

        /// Calculate pressure from temperature and density
	#pragma acc routine
        double calculatePressureFromTemperatureDensity(const double &T, const double &rho);

        /// Calculate internal energy from pressure, temperature and density
	#pragma acc routine
        double calculateInternalEnergyFromPressureTemperatureDensity(const double &P, const double &T, const double &rho);

        /// Calculate pressure and temperature from density and internal energy
	#pragma acc routine
        void calculatePressureTemperatureFromDensityInternalEnergy(double &P, double &T, const double &rho, const double &e);

        /// Calculate density from pressure and temperature
	#pragma acc routine
        double calculateDensityFromPressureTemperature(const double &P, const double &T);

        /// Calculate density and internal energy from pressure and temperature
	#pragma acc routine
        void calculateDensityInternalEnergyFromPressureTemperature(double &rho, double &e, const double &P, const double &T);

        /// Calculate specific heat capacities
	#pragma acc routine
        void calculateSpecificHeatCapacities(double &c_v_, double &c_p, const double &P, const double &T, const double &rho);

        /// Calculate heat capacities ratio
	#pragma acc routine
        double calculateHeatCapacitiesRatio(const double &P, const double &rho);

        /// Calculate speed of sound
	#pragma acc routine
        double calculateSoundSpeed(const double &P, const double &T, const double &rho);

        /// Calculate expansivity & compressibility
	#pragma acc routine
        double calculateVolumeExpansivity(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsothermalCompressibility(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsentropicCompressibility(const double &P, const double &T, const double &bar_v);

    protected:

        ////////// PARAMETERS //////////

        /// Thermodynamic properties
        double gamma;						/// Heat capacities ratio [-]
        double P_inf;						/// Pressure infinity (liquid stiffnes) [Pa]
        double e_0;						/// Internal energy zero reference [J/kg]
        double c_v;						/// Specific isochoric heat capacity [J/(kg·K)]

    private:

};


////////// PengRobinsonModel CLASS //////////
class PengRobinsonModel : public BaseThermodynamicModel {
   
    public:

        ////////// CONSTRUCTORS & DESTRUCTOR //////////
        PengRobinsonModel();							/// Default constructor
        PengRobinsonModel(const std::string configuration_file);		/// Parametrized constructor
        virtual ~PengRobinsonModel();						/// Destructor

	////////// GET FUNCTIONS //////////
        inline double getCriticalPressure() { return( critical_pressure ); };
        inline double getCriticalTemperature() { return( critical_temperature ); };

	////////// SET FUNCTIONS //////////
        inline void setCriticalPressure(double critical_pressure_) { critical_pressure = critical_pressure_; };
        inline void setCriticalTemperature(double critical_temperature_) { critical_temperature = critical_temperature_; };

	////////// METHODS //////////
        
        /// Read configuration (input) file written in YAML language
        void readConfigurationFile();

        /// Calculate temperature from pressure and density
	#pragma acc routine
        double calculateTemperatureFromPressureDensity(const double &P, const double &rho);

        /// Calculate temperature from pressure and density with initial guess
	#pragma acc routine
        void calculateTemperatureFromPressureDensityWithInitialGuess(double &T, const double &P, const double &rho);

        /// Calculate pressure from temperature and density
	#pragma acc routine
        double calculatePressureFromTemperatureDensity(const double &T, const double &rho);

        /// Calculate internal energy from pressure, temperature and density
	#pragma acc routine
        double calculateInternalEnergyFromPressureTemperatureDensity(const double &P, const double &T, const double &rho);

        /// Calculate pressure and temperature from density and internal energy
	#pragma acc routine
        void calculatePressureTemperatureFromDensityInternalEnergy(double &P, double &T, const double &rho, const double &e);

        /// Calculate density from pressure and temperature
	#pragma acc routine
        double calculateDensityFromPressureTemperature(const double &P, const double &T);

        /// Calculate density and internal energy from pressure and temperature
	#pragma acc routine
        void calculateDensityInternalEnergyFromPressureTemperature(double &rho, double &e, const double &P, const double &T);

        /// Calculate specific heat capacities
	#pragma acc routine
        void calculateSpecificHeatCapacities(double &c_v, double &c_p, const double &P, const double &T, const double &rho);

        /// Calculate heat capacities ratio
	#pragma acc routine
        double calculateHeatCapacitiesRatio(const double &P, const double &rho);

        /// Calculate speed of sound
	#pragma acc routine
        double calculateSoundSpeed(const double &P, const double &T, const double &rho);

        /// Calculate expansivity & compressibility
	#pragma acc routine
        double calculateVolumeExpansivity(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsothermalCompressibility(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateIsentropicCompressibility(const double &P, const double &T, const double &bar_v);

        /// Calculate molar internal energy from pressure, temperature and molar volume
	#pragma acc routine
        double calculateMolarInternalEnergyFromPressureTemperatureMolarVolume(const double &P, const double &T, const double &bar_v);

        /// Calculate attractive-forces a coefficient
	#pragma acc routine
        double calculate_eos_a(const double &T);

        /// Calculate first derivative of attractive-forces a coefficient
	#pragma acc routine
        double calculate_eos_a_first_derivative(const double &T);

        /// Calculate second derivative of attractive-forces a coefficient
	#pragma acc routine
        double calculate_eos_a_second_derivative(const double &T);

        /// Calculate compressibility factor
	#pragma acc routine
        double calculate_Z(const double &P, const double &T, const double &bar_v);

        /// Calculate auxiliar parameters
	#pragma acc routine
        double calculate_A(const double &P, const double &T);
	#pragma acc routine
        double calculate_B(const double &P, const double &T);
	#pragma acc routine
        double calculate_M(const double &Z, const double &B);
	#pragma acc routine
        double calculate_N(const double &eos_a_first_derivative, const double &B);

        /// Calculate standard thermodynamic variables from NASA 7-coefficient polynomial
	#pragma acc routine
        double calculateMolarStdCpFromNASApolynomials(const double &T);
	#pragma acc routine
        double calculateMolarStdEnthalpyFromNASApolynomials(const double &T);

        /// Calculate high-pressure departure functions
	#pragma acc routine
        double calculateDepartureFunctionMolarCp(const double &P, const double &T, const double &bar_v);
	#pragma acc routine
        double calculateDepartureFunctionMolarCv(const double &P, const double &T, const double &bar_v);
	#pragma acc routine
        double calculateDepartureFunctionMolarEnthalpy(const double &P, const double &T, const double &bar_v);

        /// Calculate temperature from pressure and molar volume
	#pragma acc routine
        double calculateTemperatureFromPressureMolarVolume(const double &P, const double &bar_v);

        /// Calculate thermodynamic derivatives
	#pragma acc routine
        double calculateDPDTConstantMolarVolume(const double &T, const double &bar_v);
	#pragma acc routine
        double calculateDPDvConstantTemperature(const double &T, const double &bar_v);

        /// Calculate roots of cubic polynomial
	//#pragma acc routine
        //void calculateRootsCubicPolynomial(std::complex<double> &root_1, std::complex<double> &root_2, std::complex<double> &root_3, double &a, double &b, double &c, double &d);
        
	// ... Modified for OpenACC
	#pragma acc routine
	void calculateRootsCubicPolynomial_(double &root_1_real, double &root_2_real, double &root_3_real, double &root_2_imag, double &root_3_imag, double &a, double &b, double &c, double &d);
    private:

        /// Nonlinear solver nested class used to obtain P & T from rho & e
        class NLS_P_T_from_rho_e : public NewtonRaphson { 

            public:

            ////////// CONSTRUCTORS & DESTRUCTOR //////////
            NLS_P_T_from_rho_e(const int &n_, double* fvec_, PengRobinsonModel &pr_model_) : NewtonRaphson(n_, fvec_), pr_model(pr_model_) {
		    // Transfer this instance to the GPU
   		    #pragma acc enter data create(this)
	    };			/// Parametrized constructor
            virtual ~NLS_P_T_from_rho_e() {
		    // Free GPU memory for this instance
                    #pragma acc exit data delete(this)
	    };															/// Destructor

	    ////////// METHODS //////////

            /// Set external (enclosing class) parameters
	    #pragma acc routine
            void setExternalParameters(const double &target_rho_, const double &target_e_, const double &P_norm_, const double &T_norm_) {

                target_rho = target_rho_;
                target_e   = target_e_;
                P_norm     = P_norm_;
                T_norm     = T_norm_;

            };

            /// Evaluates the functions (residuals) in position x ... NOT USED!
	    //#pragma acc routine
            void function_vector(double* x, double* fx) {};	

            /// Evaluates the functions (residuals) in position x
	    #pragma acc routine
            void function_vector_(double* x, double* fx) {

                /// Set state to x
                double P = x[0]*P_norm;		// Unnormalize pressure
                double T = x[1]*T_norm;		// Unnormalize temperature
		
                /// For single-component systems:
                /// P will not oscillate for supercritical thermodynamic states
                /// P will oscillate for subcritical thermodynamic states
                /// ... small oscillations close to the critical point (slightly subcritical)
                /// ... large oscillations far from the critical point (notably subcritical) -> in that case, use two-phase solver
                double molecular_weight = pr_model.getMolecularWeight(); 
                double target_molar_v   = molecular_weight/target_rho;
                double guess_rho = pr_model.calculateDensityFromPressureTemperature( P, T );
                double guess_e   = pr_model.calculateMolarInternalEnergyFromPressureTemperatureMolarVolume( P, T, target_molar_v )/molecular_weight;
 
                /// Compute fx (residuals)
                fx[0] = ( guess_rho - target_rho )/( fabs( target_rho ) + 1.0e-14 );	/// function normalized
                fx[1] = ( guess_e - target_e )/( fabs( target_e ) + 1.0e-14 );		/// function normalized

            };
   
            /// Numerical recipes in C++ Jacobian method
	    #pragma acc routine	    
	    void fdjac_(double* x_, double** df_) {
	    
	        /// Globally Convergent Methods for Nonlinear Systems of Equations: fdjac method
	        /// W. H. Press, S. A. Teukolsky, W. T. Vetterling, B. P. Flannery.
	        /// Numerical recipes in C++.
	        /// Cambridge University Press, 2001.
	    
	        //const double EPS = 1.0e-8;
	        const double EPS = 1.0e-6;
	        double h, temp;
	        int i, j;
	    
	        for( j = 0; j < n; j++ ) {
	          temp = x_[j];
	          h = EPS*fabs( temp );
	          if( h == 0.0 ) h = EPS;
	          x_[j] = temp + h;
	          h = x_[j] - temp;
	          function_vector_(x_,fdjac_f);
	          x_[j] = temp;
	          for( i = 0; i < n; i++ ) {
	            df_[i][j] = ( fdjac_f[i] - fvec[i] )/h;
	          }
	        }   
	    
	    };
	    
	    /// Numerical recipes in C++ fmin method
	    #pragma acc routine
	    double fmin_(double* x_) {
	    
	        /// Globally Convergent Methods for Nonlinear Systems of Equations: fmin method
	        /// W. H. Press, S. A. Teukolsky, W. T. Vetterling, B. P. Flannery.
	        /// Numerical recipes in C++.
	        /// Cambridge University Press, 2001.
	    
	        int i;
	        double sum = 0.0;
	    
	        function_vector_(x_,fvec);
	        for( i = 0; i < n; i++ ) sum += SQR( fvec[i] );
	    
	        return( 0.5*sum );
	    
	    };
	    
	    /// Numerical recipes in C++ linear search
	    #pragma acc routine
	    void lnsrch_(double* xold_, double &fold, double* g_, double* p_, double* x_,double &f, double &stpmax, bool &check) {
	    
	        /// Globally Convergent Methods for Nonlinear Systems of Equations: linear search
	        /// W. H. Press, S. A. Teukolsky, W. T. Vetterling, B. P. Flannery.
	        /// Numerical recipes in C++.
	        /// Cambridge University Press, 2001.
	    
	        const double ALF = 1.0e-4, TOLX = std::numeric_limits<double>::epsilon();
	        double a, alam, alam2 = 0.0, alamin, b, disc, f2 = 0.0;
	        double rhs1, rhs2, slope = 0.0, sum = 0.0, temp, test, tmplam;
	        int i;
	    
	        check = false;
	        for( i = 0; i < n; i++ ) sum += p_[i]*p_[i];
	        sum = sqrt( sum );
	        if( sum > stpmax ) {
	            for( i = 0; i < n; i++ ) p_[i] *= stpmax/sum;
	        }
	        for( i = 0; i < n; i++ ) slope += g_[i]*p_[i];
	        //if(slope >= 0.0) cout << "Roundoff problem in lnsrch" << endl;
	        test = 0.0;
	        for( i = 0; i < n; i++ ) {
	            temp = fabs( p_[i] )/std::max( fabs( xold_[i] ), 1.0 );
	            if( temp > test ) test = temp;
	        }
	        alamin = TOLX/test;
	        alam = 1.0;
	        for(;;) {
	            for( i = 0; i < n; i++ ) x_[i] = xold_[i] + alam*p_[i];
	            f = fmin_( x_ );
	            if( alam < alamin ) {
	                for( i = 0; i < n; i++ ) x_[i] = xold_[i];
	                check = true;
	                return;
	    	    } else if( f <= fold + ALF*alam*slope ) {
	                return;
	    	    } else {
	                if( alam == 1.0 ) {
	    	        tmplam = -slope/( 2.0*( f - fold - slope ) );
	    	        } else {
	    	        rhs1 = f - fold - alam*slope;
	    	        rhs2 = f2 - fold - alam2*slope;
	    	        a = ( rhs1/( alam*alam ) - rhs2/( alam2*alam2 ) )/( alam - alam2 );
	    	        b = ( -alam2*rhs1/( alam*alam ) + alam*rhs2/( alam2*alam2 ) )/( alam - alam2 );
	    	        if( a == 0.0 ) {
	                        tmplam = -slope/(2.0*b);
	    	        } else {
	    	            disc = b*b - 3.0*a*slope;
	    		    if( disc < 0.0 ) tmplam = 0.5*alam;
	    		    else if( b <= 0.0 ) tmplam = ( -b + sqrt( disc ) )/( 3.0*a );
	    		    else tmplam = -slope/( b + sqrt( disc ) );
	    	        }
	    	        if( tmplam > 0.5*alam ) tmplam = 0.5*alam;
	    	    }
	            }
	            alam2 = alam;
	            f2 = f;
	            alam = std::max( tmplam, 0.1*alam );
	        }
	    
	    };
	    
	    /// Runs the minimization algorithm, calculating the minimum (fxmin) and its independent variables (xmin)
	    #pragma acc routine
	    void solve_(double &fxmin, double* xmin, const int &max_iter, int &iter, const double &tolerance, const double &MX_STP) {
	    
	        /// Newton-Raphson method using approximated derivatives:
	        /// W. H. Press, S. A. Teukolsky, W. T. Vetterling, B. P. Flannery.
	        /// Numerical recipes in C++.
	        /// Cambridge University Press, 2001.
	    
	        //const int MAXITS=200;
	        const int MAXITS=max_iter;
	        //const double TOLF=1.0e-8, TOLX=std::numeric_limits<double>::epsilon(), STPMX=100.0, TOLMIN=1.0e-12;
	        const double TOLF=tolerance, TOLX=std::numeric_limits<double>::epsilon(), STPMX=MX_STP, TOLMIN=1.0e-12;
	        int i, j;
	        bool check;
	        double den,d,f,fold,stpmax,sum,temp,test;
	    
	        f = fmin_( xmin );
	        test = 0.0;
	        for(i = 0; i < n; i++) {
	          if( fabs( fvec[i] ) > test ) fxmin = test = fabs( fvec[i] );
	        }
	        if( test < 0.01*TOLF ) {
	          check = false;
	          return;
	        }
	        sum = 0.0;
	        for(i = 0; i < n; i++) sum += SQR( xmin[i] );
	        stpmax = STPMX*std::max( sqrt( sum ), double( n ) );
	        for(iter = 0; iter < MAXITS; iter++) {
	          fdjac_(xmin,fjac);
	          for( i = 0; i < n; i++ ) {
	            sum = 0.0;
	    	    for( j = 0; j < n; j++ ) sum += fjac[j][i]*fvec[j];
	    	    g[i] = sum;
	          }
	          for(i = 0; i < n; i++) xold[i] = xmin[i];
	          fold = f;
	          for(i = 0; i < n; i++) p[i] = -fvec[i];
	          ludcmp(fjac,indx,d);
	          lubksb(fjac,indx,p);
	          lnsrch_(xold,fold,g,p,xmin,f,stpmax,check);
	          test = 0.0;
	          for(i = 0; i < n; i++) {
	            if( fabs( fvec[i] ) > test ) fxmin = test = fabs( fvec[i] );
	          }
	          if( test < TOLF ) {
	            check = false;
	            //cout << "Newton-Raphson's minimization TOLF error: " << test << " ( iteration: " << iter << " )" << endl;
	    	    return;
	          }
	          if(check) {
	            test = 0.0;
	    	  den = std::max( f, 0.5*n );
	    	  for( i = 0; i < n; i++ ) {
	    	    temp = fabs( g[i])*std::max( fabs( xmin[i] ), 1.0 )/den;
	    	    if( temp > test ) test = temp;
	    	  }
	    	  check = ( test < TOLMIN );
	    	    return;
	          }
	          test = 0.0;
	          for(i = 0; i < n; i++) {
	    	    temp = ( fabs( xmin[i] - xold[i] ) )/std::max( fabs(xmin[i]), 1.0 );
	    	    if( temp > test ) fxmin = test = temp;
	          }
	          if( test < TOLX ) {
	            //cout << "Newton-Raphson's minimization TOLX error: " << test << " ( iteration: " << iter << " )" << endl;
	    	    return;
	          }
	        }
	        //cout << "MAXITS exceeded in Newton-Raphson" << endl;
	    
	    };

            ////////// PARAMETERS //////////

	    /// External (enclosing) parameters
            double target_rho;			/// External (enclosing class) target density
            double target_e;			/// External (enclosing class) target specific internal energy
            double P_norm;			/// External (enclosing class) pressure normalization
            double T_norm;			/// External (enclosing class) temperature normalization

	    /// External (enclosing) class
            PengRobinsonModel &pr_model;	/// Reference to PengRobinsonModel (enclosing) class
 
        };

    protected:

        ////////// PARAMETERS //////////

        /// Thermodynamic properties
        double acentric_factor;					/// Acentric factor [-]
        double critical_temperature;				/// Critical temperature [K]
        double critical_pressure;				/// Critical pressure [Pa]
        double critical_molar_volume;				/// Critical molar volume [m3/mol]
        double NASA_coefficients[15];				/// NASA 7-coefficient polynomial

        /// Equation of state (EoS) parameters
        //double eos_a;						/// EoS attractive-forces coefficient
        double eos_b;						/// EoS finite-pack-volume coefficient
        double eos_ac, eos_kappa;				/// EoS dimensionless parameters

        /// Aitken's delta-squared process parameters
        int aitken_max_iter              = 100;			/// Maximum number of iterations
        double aitken_relative_tolerance = 1.0e-5;		/// Relative tolerance

        /// Nonlinear P-T solver parameters
        int nls_PT_max_iter              = 100;			/// Maximum number of iterations
        double nls_PT_relative_tolerance = 1.0e-5;		/// Relative tolerance
        //double nls_PT_STPMX              = 1.0e2;		/// Scaled maximum step length allowed in line searches
        double nls_PT_STPMX              = 1.0e-3;		/// Scaled maximum step length allowed in line searches
        NLS_P_T_from_rho_e *nls_PT_solver;			/// Pointer to NLS_P_T_from_rho_e
	double* nls_PT_unknowns;				/// NLS_P_T_from_rho_e unknowns: P & T
	double* nls_PT_r_vec;					/// NLS_P_T_from_rho_e vector of functions residuals

    private:

};

#endif /*_THERMODYNAMIC_MODEL_*/
