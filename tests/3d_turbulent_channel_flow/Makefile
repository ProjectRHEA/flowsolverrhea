EXECUTABLE   = RHEA.exe
MAIN         = myRHEA.cpp
CONFIG_FILE  = configuration_file.yaml
PROJECT_PATH = $(RHEA_PATH)
SRC_DIR      = $(PROJECT_PATH)/src
CXX          = mpicxx
# CPU FLAGS
#CXXFLAGS     = -O3 -Wall -std=c++0x -Wno-unknown-pragmas -Wno-unused-variable -I$(PROJECT_PATH)
CXXFLAGS     = -Ofast -Wall -std=c++0x -Wno-unknown-pragmas -Wno-unused-variable -I$(PROJECT_PATH)
## CPU-GPU FLAGS
#CXXFLAGS     = -fast -acc -gpu=cc75,mem:separate:pinnedalloc -Minfo=accel -O3 -Wall -std=c++0x -I$(PROJECT_PATH)
# CUDA_AWARE MPI FLAG
CXXFLAGS += -D_GPU_AWARE_MPI_DEACTIVATED_=1
# UBUNTU - LINUX
INC_LIB_YAML =
INC_DIR_YAML =
INC_LIB_HDF5 = -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi 
INC_DIR_HDF5 = -I/usr/include/hdf5/openmpi
## MAC - OS X
#INC_LIB_YAML = -L/usr/local/lib
#INC_DIR_YAML = -I/usr/local/include
#INC_LIB_HDF5 =
#INC_DIR_HDF5 =
LDFLAGS      = -lyaml-cpp -lhdf5



# !! THE LINES BELOW SHOULD NOT BE MODIFIED !! #

OBJS = $(SRC_DIR)/*.cpp
INC_LIB = $(INC_LIB_YAML) $(INC_LIB_HDF5)
INC_DIR = $(INC_DIR_YAML) $(INC_DIR_HDF5)

all: preprocess build postprocess

# ! OPENACC STUFF TO AVOID PROBLEMS WITH POINTERS ... CAN BE REMOVED ONCE OPENACC ACCEPTS POINTERS WITH DERIVED CLASSES ! #
EXTRACTED_THERMODYNAMIC_MODEL := $(shell grep "thermodynamic_model" $(CONFIG_FILE) | sed -n "s/.*'\([^']*\)'.*/\1/p")
THERMODYNAMIC_MODEL = dummy
ifeq ($(EXTRACTED_THERMODYNAMIC_MODEL), IDEAL_GAS)
	THERMODYNAMIC_MODEL = IdealGasModel
else ifeq ($(EXTRACTED_THERMODYNAMIC_MODEL), STIFFENED_GAS)
	THERMODYNAMIC_MODEL = StiffenedGasModel
else ifeq ($(EXTRACTED_THERMODYNAMIC_MODEL), PENG_ROBINSON)
	THERMODYNAMIC_MODEL = PengRobinsonModel
endif
EXTRACTED_TRANSPORT_COEFFICIENTS_MODEL := $(shell grep "transport_coefficients_model" $(CONFIG_FILE) | sed -n "s/.*'\([^']*\)'.*/\1/p")
TRANSPORT_COEFFICIENTS_MODEL = dummy
ifeq ($(EXTRACTED_TRANSPORT_COEFFICIENTS_MODEL), CONSTANT)
        TRANSPORT_COEFFICIENTS_MODEL = ConstantTransportCoefficients
else ifeq ($(EXTRACTED_TRANSPORT_COEFFICIENTS_MODEL), LOW_PRESSURE_GAS)
        TRANSPORT_COEFFICIENTS_MODEL = LowPressureGasTransportCoefficients
else ifeq ($(EXTRACTED_TRANSPORT_COEFFICIENTS_MODEL), HIGH_PRESSURE)
        TRANSPORT_COEFFICIENTS_MODEL = HighPressureTransportCoefficients
endif
EXTRACTED_RIEMANN_SOLVER_SCHEME := $(shell grep "riemann_solver_scheme" $(CONFIG_FILE) | sed -n "s/.*'\([^']*\)'.*/\1/p")
RIEMANN_SOLVER_SCHEME = dummy
ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), DIVERGENCE)
        RIEMANN_SOLVER_SCHEME = DivergenceFluxApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), MURMAN-ROE)
        RIEMANN_SOLVER_SCHEME = MurmanRoeFluxApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), KGP)
        RIEMANN_SOLVER_SCHEME = KgpFluxApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), SHIMA)
        RIEMANN_SOLVER_SCHEME = ShimaFluxApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), HLL)
        RIEMANN_SOLVER_SCHEME = HllApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), HLLC)
        RIEMANN_SOLVER_SCHEME = HllcApproximateRiemannSolver
else ifeq ($(EXTRACTED_RIEMANN_SOLVER_SCHEME), HLLC+)
        RIEMANN_SOLVER_SCHEME = HllcPlusApproximateRiemannSolver
endif
EXTRACTED_RUNGE_KUTTA_METHOD := $(shell grep "runge_kutta_time_scheme" $(CONFIG_FILE) | sed -n "s/.*'\([^']*\)'.*/\1/p")
EXPLICIT_RUNGE_KUTTA_METHOD = dummy
ifeq ($(EXTRACTED_RUNGE_KUTTA_METHOD), RK1)
	EXPLICIT_RUNGE_KUTTA_METHOD = RungeKutta1Method
else ifeq ($(EXTRACTED_RUNGE_KUTTA_METHOD), SSP-RK2)
	EXPLICIT_RUNGE_KUTTA_METHOD = StrongStabilityPreservingRungeKutta2Method
else ifeq ($(EXTRACTED_RUNGE_KUTTA_METHOD), SSP-RK3)
	EXPLICIT_RUNGE_KUTTA_METHOD = StrongStabilityPreservingRungeKutta3Method
endif
#EXTRACTED_IMMERSED_BOUNDARY_METHOD := $(shell grep "immersed_boundary_method" $(CONFIG_FILE) | sed -n "s/.*'\([^']*\)'.*/\1/p")
#IMMERSED_BOUNDARY_METHOD = dummy
IMMERSED_BOUNDARY_METHOD = BaseImmersedBoundaryMethod

# ! OPENACC STUFF TO AVOID PROBLEMS WITH POINTERS ... CAN BE REMOVED ONCE OPENACC ACCEPTS POINTERS WITH DERIVED CLASSES ! #
preprocess:
	@cp $(SRC_DIR)/FlowSolverRHEA.hpp $(SRC_DIR)/FlowSolverRHEA.hpp.original
	@cp $(SRC_DIR)/FlowSolverRHEA.cpp $(SRC_DIR)/FlowSolverRHEA.cpp.original
	@for file in $(SRC_DIR)/FlowSolverRHEA.*pp; do \
		sed -i 's/\b_ThermodynamicModel_\b/$(THERMODYNAMIC_MODEL)/g' $$file; \
		sed -i 's/\b_TransportCoefficientsModel_\b/$(TRANSPORT_COEFFICIENTS_MODEL)/g' $$file; \
		sed -i 's/\b_RiemannSolverScheme_\b/$(RIEMANN_SOLVER_SCHEME)/g' $$file; \
		sed -i 's/\b_ExplicitRungeKuttaMethod_\b/$(EXPLICIT_RUNGE_KUTTA_METHOD)/g' $$file; \
		sed -i 's/\b_ImmersedBoundaryMethod_\b/$(IMMERSED_BOUNDARY_METHOD)/g' $$file; \
	done

build: $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXECUTABLE) $(MAIN) $(OBJS) $(INC_LIB) $(INC_DIR) $(LDFLAGS)


# ! OPENACC STUFF TO AVOID PROBLEMS WITH POINTERS ... CAN BE REMOVED ONCE OPENACC ACCEPTS POINTERS WITH DERIVED CLASSES ! #
postprocess:
	@mv $(SRC_DIR)/FlowSolverRHEA.hpp.original $(SRC_DIR)/FlowSolverRHEA.hpp
	@mv $(SRC_DIR)/FlowSolverRHEA.cpp.original $(SRC_DIR)/FlowSolverRHEA.cpp

.PHONY: clean
clean:
	$(RM) $(EXECUTABLE) *.o

